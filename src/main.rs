use std::io::{self, BufRead, Write};
use std::fs::{File, OpenOptions};
use std::env;
use std::path::Path;

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path> {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn add_content(content: &str) -> anyhow::Result<()> {
    let mut f = OpenOptions::new()
        .write(true)
        .append(true)
        .open("vkBasalt.conf")?;

    if let Err(err) = f.write_all(format!("{content}\n").as_bytes()) {
        return Err(anyhow::anyhow!("{err}"));
    }

    Ok(())
}

fn fix_filenames(real_filename: &str) -> &str {
    // That is what ReShade generated
    if real_filename == "MXAO" {
        "qUINT_mxao"
    }
    else if real_filename == "CA" {
        "ChromaticAberration"
    }
    else if real_filename == "AdvancedCRT" {
        "CRT"
    }
    else if real_filename == "After" || real_filename == "Before" {
        "Splitscreen"
    }
    else if real_filename == "AspectRatioPS" {
        "AspectRatio"
    }
    else if real_filename == "BloomAndLensFlares" {
        "Bloom"
    }
    else if real_filename == "ChromaticAberration" {
        "Prism"
    }
    else if real_filename == "Directionally_Localized_Anti_Aliasing" {
        "DLAA"
    }
    else if real_filename == "GlitchB" {
        "Glitch"
    }
    else if real_filename == "GP65CJ042DOF" {
        "DOF"
    }
    else if real_filename == "HDR" {
        "FakeHDR"
    }
    else if real_filename == "KNearestNeighbors" || real_filename == "NonLocalMeans" {
        "Denoise"
    }
    else if real_filename == "LeiFx_Tech" {
        "3DFX"
    }
    else if real_filename == "LightDoF_AutoFocus" || real_filename == "LightDoF_Far" || real_filename == "LightDoF_Near" {
        "LightDoF"
    }
    // I don't know how is this works...
    // Maybe I'm not right about this, but I don't think vkBasalt support this kind of techniques anyway
    else if real_filename == "MagicDOF" || real_filename == "MartyMcFlyDOF" || real_filename == "MatsoDOF" || real_filename == "RingDOF" {
        "DOF"
    }
    else if real_filename == "Mode1" || real_filename == "Mode2" || real_filename == "Mode3" {
        "FineSharp"
    }
    else if real_filename == "MotionBlur" {
        "FakeMotionBlur"
    }
    // Linux personal pain
    else if real_filename == "Nightvision" {
        "NightVision"
    }
    else if real_filename == "PPFXBloom" {
        "PPFX_Bloom"
    }
    else if real_filename == "PPFXSSDO" {
        "PPFX_SSDO"
    }
    // This doens't looks right... But this is what ReShade generates
    else if real_filename == "Tint" {
        "Sepia"
    }
    // Doesn't work in vkBasalt, but leave it 'just in case'
    else if real_filename == "UIDetect_After" || real_filename == "UIDetect_Before" {
        "UIDetect"
    }
    else if real_filename == "UIMask_Bottom" || real_filename == "UIMask_Top" {
        "UIMask"
    }
    else {
        real_filename
    }
}

fn parse_file(path: &Path) -> anyhow::Result<()> {
    let share = dirs::data_dir().unwrap().to_string_lossy().to_string();

    // See: https://github.com/gripped/vkBasalt-working-reshade-shaders
    let known_broken_shaders: Vec<&'static str> = vec![
        "BeforeAfter", "Deband", "GaussianBlur", "UIDetect"
    ];

    if let Ok(lines) = read_lines(path) {
        for line in lines {
            if let Ok(line) = line {
                if line.contains("PreprocessorDefinitions") || line.contains("TechniqueSorting") {
                    continue;
                }
                else if line.contains("[") && line.contains("]") {
                    let mut file_name = line.replace("[", "");
                    file_name = file_name.replace("]", "");
                    add_content(&format!("; {file_name}"))?;
                }
                else if line.is_empty() {
                    add_content("\n")?;
                }
                else if line.contains("Techniques") {
                    let ts: Vec<&str> = line.split("=").collect();
                    let techniques: Vec<&str> = ts[1].split(",").collect();
                    let mut techniques_included: Vec<String> = vec![];

                    for item in techniques {
                        // Fix some quirks
                        let real_filename = fix_filenames(item);

                        if !Path::new(&format!("{share}/vkBasalt/reshade-shaders/shaders/{real_filename}.fx")).exists() {
                            println!("Warning: '{item}' doesn't exists! Skip.");
                            continue;
                        }
                        else if known_broken_shaders.contains(&item) {
                            println!("Warning: '{item}' known to be broken! Skip.");
                            continue;
                        }

                        techniques_included.push(item.to_string());
                        add_content(&format!("{item} = {share}/vkBasalt/reshade-shaders/shaders/{real_filename}.fx"))?;
                    }

                    add_content(&format!("\neffects = {}", techniques_included.join(":")))?;
                }
                else {
                    add_content(&line)?;
                }
            }
        }
    }

    Ok(())
}

fn main() -> anyhow::Result<()> {
    let args: Vec<String> = env::args().collect();

    if args.len() <= 1 {
        return Err(anyhow::anyhow!("No command."));
    }

    let preset = std::path::Path::new(args[1].as_str());

    if !preset.exists() {
        return Err(anyhow::anyhow!("Failed to load preset."));
    }

    File::create("vkBasalt.conf").ok();

    let share = dirs::data_dir().unwrap().to_string_lossy().to_string();
    add_content(&format!("reshadeTexturePath = {share}/vkBasalt/reshade-shaders/textures"))?;
    add_content(&format!("reshadeIncludePath = {share}/vkBasalt/reshade-shaders/shaders"))?;
    add_content("toggleKey = Home\n")?;

    parse_file(preset)?;

    println!("Successfuly create vkBasalt.conf from Reshade preset.");

    Ok(())
}
