# Reshade2Basalt

Converts Reshade presets to vkBasalt.

### NOTE

Not all shaders works with vkBasalt. At this moment, most presets can be converted seemlisly, but some requires fixes.

While most values can be copy-pasted, some of them (vec2, vec3, vec4) requires changes in shaders itself.

You can check if shader is supported [here](https://github.com/gripped/vkBasalt-working-reshade-shaders). Unsupported shaders will be disabled by this program. At this moment unsupported is:

- BeforeAfter
- Deband
- GaussianBlur
- UIDetect

### Installation

Download shaders:

```bash
git clone https://github.com/crosire/reshade-shaders.git
cd reshade-shaders
git checkout nvidia
cd ..
```

And copy them:

```bash
mkdir -p ~/.local/share/vkBasalt/reshade-shaders/{shaders,textures}
cp -r reshade-shaders/ShadersAndTextures/*.png ~/.local/share/vkBasalt/reshade-shaders/texture
cp -r reshade-shaders/ShadersAndTextures/{*.fx,*.fxh} ~/.local/share/vkBasalt/reshade-shaders/shaders
```

### License

GPL v3.0
